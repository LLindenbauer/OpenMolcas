%! dynamix.tex $ this file belongs to the Molcas repository $

\section{\program{dynamix}}
\label{UG:sec:dynamix}
\index{Program!Dynamix@\program{Dynamix}}\index{Dynamix@\program{Dynamix}}
%%%<MODULE NAME="DYNAMIX">
%%Description:
%%%<HELP>
%%+The DYNAMIX program allows to do molecular dynamics simulations using
%%+the velocity Verlet algorithm. It has also the capability to detect
%%+non-adiabatic transition using a surface hopping algorithm.
%%%</HELP>

The \program{DYNAMIX} program performs molecular dynamics (MD)
simulations in \molcas. Here the nuclei are moved according to the
classical Newton's equations which are solved numerically using the
velocity Verlet algorithm\cite{swope:637}. The algorithm requires
coordinates, velocities and forces as input. \program{DYNAMIX} can be
used with any electronic structure method in \molcas. Also environmental
effects can be taken into account in the MD simulation: the solvent can be
considered implicitly using the reaction field keyword in \program{GATEWAY}
or explicitly in hybrid QM/MM calculation which requires the \program{ESPF}
program.

When multiple electronic states are involved in a MD simulation, a trajectory
surface hopping (TSH) algorithm allows non-adiabatic transitions between
different states. This TSH algorithm evaluates the change of the wavefunction
along the trajectory and induces a hop if certain criteria a met (for further
details read the \program{RASSI} section). In the current implementation the
surface hopping algorithm can be used only with state averaged CASSCF
wavefunction. However, an extension for CASPT2 and other methods are in preparation.

The Tully algorithm is available in a separate module \program{Surfacehop}.

\subsection{Dependencies}
\label{UG:sec:dynamix_dependencies}
The coordinates and the forces are required by the \program{DYNAMIX} program.
\program{DYNAMIX} reads the initial coordinates from the \file{RUNFILE} and
updates them in each iteration. In addition \program{DYNAMIX} depends on the
\program{ALASKA} program, since it generates forces.

\subsection{Files}
\label{UG:sec:dynamix_files}

\subsubsection{Input files}
\label{UG:sec:dynamix_inp_files}

\begin{filelist}
\item[velocity.xyz]
Contains the initial velocities of the MD simulation.
\end{filelist}

\subsubsection{Output files}
\label{UG:sec:dynamix_output_files}

\begin{filelist}
\item[RUNFILE]
Trajectory information such as current time, velocities, etc.\ are stored in this file.
\item[md.xyz]
The coordinates for each step of the MD trajectory are saved here.
\item[md.energies]
The potential, kinetic and total energies are written to this file. In case of multiple
electronic states, the energies of all roots are saved.
\end{filelist}

\subsection{Input}
\label{UG:sec:dynamix_inp}

This section describes the input syntax of \program{DYNAMIX} in the \molcas\ program
package. In general a MD simulation requires a \keyword{FOREACH} loop which contains
several programs to compute the energy and \program{ALASKA} for subsequent gradient
computation. The input of the \program{DYNAMIX} begins with the program name,
and is followed by the only compulsory keyword \keyword{VELV} which specifies the
velocity Verlet algorithm:
\begin{inputlisting}
&DYNAMIX
 VELV
\end{inputlisting}

\subsubsection{General keywords}
\begin{keywordlist}
%---
\item[VELVerlet]
%%%<KEYWORD MODULE="DYNAMIX" NAME="VELVER" APPEAR="Velocity Verlet algorithm" KIND="SINGLE" LEVEL="BASIC">
%%Keyword: VELVerlet <basic>
%%%<HELP>
%%+Specifies the velocity Verlet algorithm for MD simulation.
%%%</HELP></KEYWORD>
This keyword specifies the velocity Verlet algorithm \cite{swope:637} to solve Newton's
equations of motion. It's the only compulsory keyword in the program.
%---
\item[DTime]
%%%<KEYWORD MODULE="DYNAMIX" NAME="DT" APPEAR="Time step" KIND="REAL" LEVEL="BASIC" DEFAULT_VALUE="10.0" MIN_VALUE="0.0">
%%Keyword: DTime <advanced>
%%%<HELP>
%%+Defines the time step of the MD simulation.
%%%</HELP></KEYWORD>
Defines the $\delta$t which is the time step in the MD simulation and which is
used for the integration of Newton's equations of motion.
The program expects the time to be given in floating point
format and in atomic unit of time (1 a.u. of time = 2.42$\cdot$$10^{-17}$ s). (Default = 10).
%---
\item[VELOcities]
%%%<KEYWORD MODULE="DYNAMIX" NAME="VELO" APPEAR="Initial velocities" KIND="CHOICE" LIST="0: Zero,1: Read Cartesian,2: Read mass-weighted,3: Maxwell-Boltzmann" LEVEL="ADVANCED" DEFAULT_VALUE="0">
%%Keyword: VELOcities <advanced>
%%%<HELP>
%%+Specifies the initial velocities.
%%%</HELP></KEYWORD>
Specifies how the initial velocities are generated.
This keyword is followed by an integer on the next line. The internal
unit of the velocities is [Bohr$\cdot$(a.u. of time)$^{-1}$].
\begin{description}
\item[{\mdseries{ }0}] - Zero velocities. (Default)
\item[{\mdseries{ }1}] - The velocities are read from the file \file{\$Project.velocity.xyz}
in \file{\$WorkDir}. This file contains velocities in the xyz format given in the same
order as the atoms in coordinate file. The unit of the velocities is [Bohr$\cdot$(a.u. of time)$^{-1}$].
\item[{\mdseries{ }2}] - This option allows to read in mass-weighted velocities from the
file \file{\$Project.velocity.xyz} in [Bohr$\cdot$$\sqrt{a.m.u.}$$\cdot$(a.u. of time)$^{-1}$].
\item[{\mdseries{ }3}] - This option takes random velocities from a Maxwell-Boltzmann distribution, at
a given temperature, assuming that every component of the velocity can be considered as an independent gaussian random variable.
\end{description}
%---
\item[THERmostat]
%%%<KEYWORD MODULE="DYNAMIX" NAME="THER" APPEAR="Thermostat" KIND="CHOICE" LIST="0: No scaling,1: Constant energy,2: Nosé-Hoover" LEVEL="ADVANCED" DEFAULT_VALUE="0">
%%Keyword: THERmostat <advanced>
%%%<HELP>
%%+Keyword for temperature control.
%%%</HELP></KEYWORD>
Regulates the control of the temperature by scaling the velocities. The option
is an integer given on the next line.
\begin{description}
\item[{\mdseries{ }0}] - No velocity scaling. (Default)
\item[{\mdseries{ }1}] - The velocities are scaled in order to keep the total energy constant.
\item[{\mdseries{ }2}] - The velocities are scaled according to the Nos\'e-Hoover chain of thermostats algorithm, used to perform molecular symulation at
constant temperature, resulting in statistics belonging to the canonical ensemble (NVT).
\end{description}
%---
\item[TEMPerature]
%%%<KEYWORD MODULE="DYNAMIX" NAME="TEMP" APPEAR="Temperature of the simulation" KIND="REAL" LEVEL="ADVANCED" DEFAULT_VALUE="298.15" MIN_VALUE="0.0">
%%Keyword: TEMPerature <advanced>
%%%<HELP>
%%+Keyword to state the temperature of the simulation.
%%%</HELP></KEYWORD>
Defines the numerical value of the temperature, which is used together with the Nos\'e-Hoover
chain of thermostats to perform molecular dynamics at constant temperature. (Default = 298.15K)
%---
\item[HOP]
%%%<KEYWORD MODULE="DYNAMIX" NAME="HOP" APPEAR="Maximum number of surface hops" KIND="INT" LEVEL="BASIC" DEFAULT_VALUE="0" MIN_VALUE="0">
%%Keyword: HOP <basic>
%%%<HELP>
%%+Specifies the maximum number of transitions between electronic states.
%%%</HELP></KEYWORD>
Enables the trajectory surface hopping algorithm if the integer given in
the next line is bigger than 0. The integer also specifies how many
non-adiabatic transitions are allowed between electronic states.
%---
\item[RESTART]
%%%<KEYWORD MODULE="DYNAMIX" NAME="RESTART" APPEAR="Restart the trajectory" KIND="REAL" LEVEL="ADVANCED">
%%Keyword: RESTart <advanced>
%%%<HELP>
%%+Restarts the trajectory at a given time, which is specified on the next line.
%%%</HELP></KEYWORD>
This keyword allows to restart the trajectory at a given time.
The time is given on the next line in atomic units.
%---
\item[H5RESTART]
%%%<KEYWORD MODULE="DYNAMIX" NAME="H5RESTART" APPEAR="Restart the trajectory from a H5 file" KIND="REAL" LEVEL="ADVANCED">
%%Keyword: H5REstart <advanced>
%%%<HELP>
%%+Restarts a trajectory calculation from an HDF5 file, whose name is given on the next line.
%%%</HELP></KEYWORD>
This keyword allows to restart a trajectory calculation from an HDF5 file.
The name of the restart file is given on the next line.
%---
\end{keywordlist}

\subsubsection{Input examples}

The following example shows the input for an excited state CASSCF molecular dynamics
simulation of a methaniminium cation using the \program{DYNAMIX} program. The FOREACH loop
allows 1000 steps with 10 a.u. of time step size which leads to a total duration of
242 fs. In the \program{RASSCF} program the second root is selected for gradient
calculation using the keyword \keyword{MDRLXR}. This input assumes that the a
\file{JOBIPH} file with orbitals is already given. In each iteration the \file{JOBIPH}
is updated to achieve a fast convergence of the CASSCF wavefunction.
A Nos\'e-Hoover chain of thermostats, enabled with THERmo= 2, is used to
reproduce dynamics at constant temperature, where the initial velocities are
taken from a Maxwell-Boltzmann distribution at 300 K.

%%%To_extract{/doc/samples/ug/DYNAMIX.input}
\begin{inputlisting}
&GATEWAY
 COORD
 6
 Angstrom
 C  0.00031448  0.00000000  0.04334060
 N  0.00062994  0.00000000  1.32317716
 H  0.92882820  0.00000000 -0.49115611
 H -0.92846597  0.00000000 -0.49069213
 H -0.85725321  0.00000000  1.86103989
 H  0.85877656  0.00000000  1.86062860
 BASIS= 3-21G
 GROUP= nosym

>> FOREACH ITER in (1 .. 1000)

&SEWARD

>> IF ( $ITER = 1 )

&RASSCF
 LUMORB
 FileOrb= $Project.GssOrb
 Symmetry= 1
 Spin= 1
 nActEl= 2 0 0
 Inactive= 7
 RAS2= 2
 CIroot= 3 3 1

>> COPY $Project.JobIph $Project.JobOld

>> ENDIF

&RASSCF
 JOBIPH; CIRESTART
 Symmetry= 1
 Spin= 1
 nActEl= 2 0 0
 Inactive= 7
 RAS2= 2
 CIroot= 3 3 1
 MDRLXR= 2

>> COPY $Project.JobIph $Project.JobOld

&ALASKA

&DYNAMIX
 VELVer
 DT= 10.0
 VELO= 3
 THER= 2
 TEMP=300
 HOP= 1

>> END DO
\end{inputlisting}

\subsection{Dynamixtools}
This tool can be found into the \texttt{Tools/} folder and it will provide some general tools to manage molecular dynamics calculation. At the moment it can be used to generate intial condition (geometries and momenta) following a Boltzmann distribution, based on a frequency calculation. It is working with a \texttt{freq.molden} file (\texttt{.h5} support coming soon...).

From the command prompt:
\begin{inputlisting}
\$ python3 dynamixtools.py -h
usage: dynamixtools.py [-h] [-s SEED] [-l LABEL] -i I [-b BOL] -t TEMP

optional arguments:
  -h, --help            show this help message and exit
  -s SEED, --seed SEED  indicate the SEED to use for the generation of randoms
  -l LABEL, --label LABEL
                        label for your project (default is "geom")
  -i I, --input I       path of the frequency h5 or molden file
  -b BOL, --boltzmann BOL
                        number of initial condition following boltzmann
                        distribution (default 1)
  -t TEMP, --temperature TEMP
                        temperature in Kelvin for the initial conditions
\end{inputlisting}

Having a \texttt{water.freq.molden} file, this is the command to generate 200 initial condition using \texttt{3435432} as seed and a temperature of 300 Kelvin.

\begin{inputlisting}
\$ python3 dynamixtools.py -i water.freq.molden -t 300 -b 200 -s 3435432
\end{inputlisting}

%%%To_extract
%%%</MODULE>

